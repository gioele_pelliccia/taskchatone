﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskChat.Models
{
    public class Messaggio
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public string Tempo { get; set; }
        public string Testo { get; set; }

    }
}
