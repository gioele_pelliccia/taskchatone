﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TaskChat.Models;

namespace TaskChat.Data
{
    public class SqlMessaggio : InterfaceRepo<Messaggio>
    {
        private string stringaConnessione = "Server=DESKTOP-VULS57I\\SQLEXPRESS;Database=TaskChatOne;User Id=sharpuser;Password=cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false ";
        public bool Delete(int varId)
        {
            using(SqlConnection c=new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c;
                cmd.CommandText = "DELETE FROM Messaggio WHERE messaggioID = @varId";
                cmd.Parameters.AddWithValue("@varId", varId);

                c.Open();

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }

            }
            return false;
        }

        public IEnumerable<Messaggio> GetAll()
        {
            List<Messaggio> elenco = new List<Messaggio>();
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT messaggioID, nickname, testo, tempo FROM Messaggio";

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Messaggio temp = new Messaggio()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nickname = reader[1].ToString(),
                        Testo = reader[2].ToString(),
                        Tempo = (reader[3]).ToString()
                    };

                    elenco.Add(temp);
                }
            }

            return elenco;
        }
        public bool Insert(Messaggio obj)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Messaggio (nickname, testo, tempo) VALUES (@varNick, @varTesto, @varTempo)";
                comm.Parameters.AddWithValue("@varNick", obj.Nickname);
                comm.Parameters.AddWithValue("@varTesto", obj.Testo);
                comm.Parameters.AddWithValue("@varTempo", obj.Tempo);

                connessione.Open();
                try
                {
                    int affRows = comm.ExecuteNonQuery();
                    if (affRows > 0)
                        return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public bool Update(Messaggio obj)
        {
            using (SqlConnection c = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = c;
                comm.CommandText = "UPDATE Messaggio SET " +
                                        "nickname = @varNick, " +
                                        "testo = @varTesto, " +
                                        "tempo = @varTempo " +
                                        "WHERE messaggioId = @varId";

                comm.Parameters.AddWithValue("@varNick", obj.Nickname);
                comm.Parameters.AddWithValue("@varTesto", obj.Testo);
                comm.Parameters.AddWithValue("@varTempo", obj.Tempo);
                comm.Parameters.AddWithValue("@varId", obj.Id);

                c.Open();

                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
    }
}

       

