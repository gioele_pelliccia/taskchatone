let input_Nickname;
function salvaNick(){

    input_Nickname = $("#nickname").val();
    localStorage.setItem(JSON.parse(input_Nickname));
    
    // $.ajax(
    //     {
    //         url: "https://localhost:44382/api/chat/",
    //         method: "POST",
    //         data: JSON.stringify(nick),
    //         dataType: "json",
    //         contentType: "application/json",
    //         success: function(risposta){
    //             switch(risposta.result){
    //                 case "success":
    //                     stampaTabella();
        
                        
    //                     // $("#input_").val("");
                        
    //                     break;
    //                 case "error":
    //                     alert("Errore: " + risposta.description);
    //                     break;
    //             }
    //         },
    //         error: function(errore){
    //             console.log(errore)
    //             alert("Errore")
    //         }
    //     }
    // );
}

function salvaMessaggio(){
    
    let input_Testo = $("#messagebox").val();
    // let input_Tempo = $("#input_tempo").val();

    let messaggio = {
        nickname: localStorage.getItem(input_Nickname),
        testo: input_Testo,
        // tempo: input_Tempo
    }

    $.ajax(
        {
            url: "https://localhost:44382/api/chat/insert",
            method: "POST",
            data: JSON.stringify(messaggio),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        stampaTabella();
        
                        
                        $("#input_testo").val("");
                        
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

function stampaTabella(){
    $.ajax(
        {
            url: "https://localhost:44382/api/chat",
            method: "GET",
            success: function(response){

                let contenuto = "";
                for(let [index, item] of response.entries()){
                    contenuto += `  <tr>
                                        <td>${item.nickname}</td>
                                        <td>${item.testo}</td>
                                        <td>${item.tempo}</td>
                                      
                                    </tr>`;
                                       
                }

                $("#tableChat").html(contenuto);

            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}
$(document).ready(function () {
    stampaTabella();
    $("#btnNick").click(function (){
        salvaNick();
    })
    $("#btnInvio").click(function (e) { 
        salvaMessaggio();
    
});

});