function stampaTabella(){
    $.ajax(
        {
            url: "https://localhost:44382/api/chat",
            method: "GET",
            success: function(response){

                let contenuto = "";
                for(let [index, item] of response.entries()){
                    contenuto += `  <tr>
                                        <td>${item.nickname}</td>
                                        <td>${item.testo}</td>
                                        <td>${item.tempo}</td>
                                      
                                    </tr>`;
                                       
                }

                $("#tableChat").html(contenuto);

            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}
function salvaMessaggio(){
    let input_Testo = $("#messagebox").val();
    let messaggio = {
        // nickname: localStorage.getItem("Nick"),
        testo: input_Testo,
        // tempo: input_Tempo
    }
    $.ajax(
        {
            url: "https://localhost:44382/api/chat/insert",
            method: "POST",
            data: JSON.stringify(messaggio),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        stampaTabella();
        
                        
                        $("#messagebox").val("");
                        
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}
function salvaNick(){
    let input_Nickname=$("#nickname").val();
    localStorage.setItem("Nick",input_Nickname);
    
}

$(document).ready(function () {
    stampaTabella();
    $("#btnNick").click(function(){
        salvaNick();
    })
    $("#btnInvio").click(function(){
        salvaMessaggio();
    })
});